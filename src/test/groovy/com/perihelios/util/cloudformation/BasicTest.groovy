package com.perihelios.util.cloudformation

import spock.lang.Specification

import static Factory.CloudFormationTemplate

class BasicTest extends Specification {
	def "VPCs factory"() {
		when:
			def template = CloudFormationTemplate {
				VPCs {
					Bob {
						foo = "bar"
						Dog = Cat
					}

					Karen {
						something = true
						more = 1
						Tags = [
						    Me: developer
						]
					}
				}
			}

			println template.asJson()

		then:
			template.resourceSets[0].resources.Bob.awsType == "AWS::EC2::VPC"
			template.resourceSets[0].resources.Bob.name == "Bob"
			template.resourceSets[0].resources.Bob.properties.foo == "bar"
			template.resourceSets[0].resources.Bob.properties.Dog == new Reference("Cat")

			template.resourceSets[0].resources.Karen.awsType == "AWS::EC2::VPC"
			template.resourceSets[0].resources.Karen.name == "Karen"
			template.resourceSets[0].resources.Karen.properties.something == true
			template.resourceSets[0].resources.Karen.properties.more == 1
	}
}
