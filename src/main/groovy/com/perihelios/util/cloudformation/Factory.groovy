package com.perihelios.util.cloudformation

import org.slf4j.Logger
import org.slf4j.LoggerFactory

class Factory {
	private static final Logger LOG = LoggerFactory.getLogger(Factory)

	static CloudFormationTemplate CloudFormationTemplate(Closure closure) {
		CloudFormationTemplate template = new CloudFormationTemplate()

		closure.delegate = template
		closure.resolveStrategy = Closure.DELEGATE_ONLY
		closure.call()

		return template
	}

	static ResourceSet VPCs(Closure closure) {
		ResourceSet resourceSet = new ResourceSet("AWS::EC2::VPC")

		LOG.debug("Created $resourceSet.awsType resource set")

		invokeClosureOnDelegate(closure, resourceSet)

		return resourceSet
	}

	private static void invokeClosureOnDelegate(Closure c, ResourceSet resourceSet) {
		c.delegate = resourceSet
		c.resolveStrategy = Closure.DELEGATE_ONLY
		c.call()
	}
}
