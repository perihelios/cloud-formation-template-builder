package com.perihelios.util.cloudformation

import com.fasterxml.jackson.core.Version
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.module.SimpleModule
import org.slf4j.Logger
import org.slf4j.LoggerFactory

import static com.perihelios.util.cloudformation.Util.asClosure

class CloudFormationTemplate {
	private static final Logger LOG = LoggerFactory.getLogger(CloudFormationTemplate)
	private static final ObjectMapper MAPPER = new ObjectMapper()

	static {
		SimpleModule module = new SimpleModule("CloudFormationTemplate", new Version(1, 0, 0, null))
		module.addSerializer(CloudFormationTemplate, new JsonSerializer())
		MAPPER.registerModule(module)
	}

	final List<ResourceSet> resourceSets = new ArrayList<>(64)

	def methodMissing(String name, def args) {
		Closure closure = asClosure(args)
		MetaMethod method = Factory.metaClass.pickMethod(name, Closure)

		if (!method) {
			throw new NoSuchMethodException("No factory method \"$name\" found on $Factory.name")
		}

		ResourceSet resourceSet = method.invoke(null, closure) as ResourceSet
		resourceSets << resourceSet

		return resourceSet
	}

	String asJson() {
		MAPPER.writerWithDefaultPrettyPrinter().writeValueAsString(this)
	}
}
