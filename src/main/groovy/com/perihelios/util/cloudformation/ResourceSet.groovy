package com.perihelios.util.cloudformation

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import static com.perihelios.util.cloudformation.Util.asClosure

class ResourceSet {
	private static final Logger LOG = LoggerFactory.getLogger(ResourceSet)

	final Map<String, PropertySet> resources = new LinkedHashMap<>(64)
	final String awsType

	ResourceSet(String awsType) {
		this.awsType = awsType
	}

	def methodMissing(String name, Object args) {
		NamedPropertySet propertySet = new NamedPropertySet(awsType, name)
		Closure c = asClosure(args)

		LOG.debug("Created resource $propertySet")

		c.delegate = propertySet
		c.resolveStrategy = Closure.DELEGATE_ONLY
		c.call()

		resources.put(name, propertySet)
	}
}
