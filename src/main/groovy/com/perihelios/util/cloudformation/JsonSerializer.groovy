package com.perihelios.util.cloudformation

import com.fasterxml.jackson.core.JsonGenerator
import com.fasterxml.jackson.databind.SerializerProvider
import com.fasterxml.jackson.databind.ser.std.StdSerializer

import java.util.Map.Entry

class JsonSerializer extends StdSerializer<CloudFormationTemplate> {
	private static final String VERSION = "2010-09-09"

	JsonSerializer() {
		super(CloudFormationTemplate.class)
	}

	@Override
	void serialize(CloudFormationTemplate template, JsonGenerator generator, SerializerProvider provider) {
		generator.writeStartObject()
		generator.writeStringField("AWSTemplateFormatVersion", VERSION)
		generator.writeObjectFieldStart("Resources")

		for (ResourceSet resourceSet : template.resourceSets) {
			for (Entry<String, PropertySet> entry : resourceSet.resources.entrySet()) {
				String propertySetName = entry.key
				PropertySet propertySet = entry.value

				generator.writeObjectFieldStart(propertySetName)
				generator.writeStringField("Type", propertySet.awsType)

				generator.writeObjectFieldStart("Properties")

				for (Entry<String, Object> property : propertySet.properties) {
					String name = property.key
					Object value = property.value

					if (value instanceof Reference) {
						generator.writeObjectFieldStart(name)
						generator.writeStringField("Ref", value.referencedObjectName)
						generator.writeEndObject()
					} else if (value instanceof Number) {
						generator.writeObjectField(name, value)
					} else if (value instanceof Collection) {
						// TODO: Write this
					}
				}

				generator.writeEndObject()
				generator.writeEndObject()
			}
		}

		generator.writeEndObject()
	}
}
