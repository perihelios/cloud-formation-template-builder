package com.perihelios.util.cloudformation

class NamedPropertySet extends PropertySet {
	final String name

	NamedPropertySet(String awsType, String name) {
		super(awsType)
		this.name = name
	}

	@Override
	public String toString() {
		return "$awsType($id: \"$name\")"
	}
}
