package com.perihelios.util.cloudformation

import org.slf4j.Logger
import org.slf4j.LoggerFactory

class PropertySet {
	private static final Logger LOG = LoggerFactory.getLogger(PropertySet)

	final int id = Sequence.next()
	final Map<String, Object> properties = new LinkedHashMap<>(32)
	final String awsType

	PropertySet(String awsType) {
		this.awsType = awsType
	}

	@Override
	Object getProperty(String propertyName) {
		if (hasProperty(propertyName)) {
			return metaClass.getProperty(this, propertyName)
		}

		Reference reference = new Reference(propertyName)

		LOG.debug("Created $reference")

		return reference
	}

	@Override
	void setProperty(String propertyName, Object value) {
		properties.put(propertyName, value)

		LOG.debug("Set property \"$propertyName\" on $this to: $value")
	}

	@Override
	public String toString() {
		return "$awsType($id)"
	}
}
