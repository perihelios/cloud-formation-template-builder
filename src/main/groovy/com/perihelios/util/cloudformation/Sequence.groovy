package com.perihelios.util.cloudformation

import java.util.concurrent.atomic.AtomicInteger

class Sequence {
	private static final AtomicInteger NUMBER = new AtomicInteger(1)

	static int next() {
		return NUMBER.getAndIncrement()
	}
}
