package com.perihelios.util.cloudformation

class Util {
	static Closure asClosure(Object args) {
		if (args == null) throw new IllegalArgumentException("Expected single-element Object[] containing a closure, but got null")
		if (!(args instanceof Object[])) throw new IllegalArgumentException("Expected single-element Object[] containing a closure, but got ${args.getClass().name}")

		Object[] array = args as Object[]

		if (array.length == 0) throw new IllegalArgumentException("Expected single-element Object[] containing a closure, but got empty Object[]")

		Object possibleClosure = array[0]

		if (!(possibleClosure instanceof Closure)) throw new IllegalArgumentException("Expected single-element Object[] containing a closure, but got single-element Object[] containing ${possibleClosure.getClass().name}")

		return possibleClosure as Closure
	}
}
