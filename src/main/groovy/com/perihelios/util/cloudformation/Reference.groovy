package com.perihelios.util.cloudformation

class Reference {
	final String referencedObjectName

	Reference(String referenceObjectName) {
		this.referencedObjectName = referenceObjectName
	}

	boolean equals(o) {
		if (this.is(o)) return true
		if (getClass() != o.getClass()) return false

		Reference reference = (Reference) o

		if (referencedObjectName != reference.referencedObjectName) return false

		return true
	}

	int hashCode() {
		return referencedObjectName.hashCode()
	}

	@Override
	public String toString() {
		return "reference(\"$referencedObjectName\")"
	}
}
